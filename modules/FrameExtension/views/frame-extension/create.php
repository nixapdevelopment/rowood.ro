<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\FrameExtension\models\FrameExtension */

$this->title = Yii::t('app', 'Create Frame Extension');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frame Extensions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frame-extension-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
