<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\modules\Order\controllers\QHelper;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\web\View;

$bundle = RowoodAssets::register($this);
?>

<div class="cart-view">

    <style>
        .cart-item-labels .centered-text{
            /*text-align: center;*/
            display: block;
            font-size: 16px;
            font-weight: 600;
        }
        .cart-item-info .info{
            /*text-align: center;*/
            display: block;
            padding-top: 15px;

        }
        .cart-item-info .image{
            display: block;
            padding: 5px;
        }
        .cart-item-info .product-title{
            font-weight: 600;
            font-size: 18px;
        }
        .cart-view .container .row{
            border: 1px solid rgba(51, 51, 51, 0.32);
            border-top: none;
        }
        .cart-item-labels{
            border-top: 1px solid rgba(51, 51, 51, 0.32)!important;
            background: rgba(51, 51, 51, 0.32);
        }

        .cart-view {
            margin-bottom: 50px;
        }
        .cart-view a{
            text-decoration: none;
            color: black;
        }
        .final-price{
            font-size: 16px;
            font-weight: 600;
            padding: 10px;
        }
        .final-price .sum{
            font-size: 18px;
            font-weight: 600;
            color: #e3210d;
        }
        .checkout-button{
            padding-top: 15px;
            margin-top: 10px;
            display: inline-block;
            width: 150px;
            height: 60px;
            background-color: #fff;
            border: 2px solid #e3210d;
            font-family: "FuturaPT-Demi" ,sans-serif;
            font-size: 20px;
            font-weight: 600;
            text-align: center;
            color: #ffffff;
        }
        .checkout-button:hover{
            border: 2px solid #e3210d;
            background-color: #e3210d;
            color: #ffffff;
        }
    </style>

    <div class="container">
        <div class="col-sm-12">
            <div class="row cart-item-labels">
                <div class="col-sm-1">
                    <span class="centered-text">Nr</span>
                </div>
                <div class="col-sm-1">
                    <span class="centered-text">Imagine</span>
                </div>
                <div class="col-sm-2">
                    <span class="centered-text">Denumire</span>
                </div>
                <div class="col-sm-1">
                    <span class="centered-text">Cantitate</span>
                </div>
                <div class="col-sm-5">
                    <span class="centered-text">Detalii</span>
                </div>
                <div class="col-sm-2">
                    <span class="centered-text">Pret</span>
                </div>
            </div>
            <?php Pjax::begin([
                    'id' => 'p1',
            ])?>
            <?php foreach ($products as $index => $product) { ?>


                <?=Html::beginForm('/admin/ecommerce/order/cart/add-to-cart/','get',['class' => 'quantity-form','id'=>'cart-item'.$product['id']])?>

                <?=Html::hiddenInput('return',true)?>
                <?=Html::hiddenInput('id',$product['id'])?>
            <div class="row cart-item-info">
                    <div class="col-sm-1 info ">
                        <?=$index+1?><br><input type="button" onclick="deleteThis(this.form)" value="X" style="position: absolute; font-weight: 600; color: red; margin-top: 10px;margin-left: 50px;">
                    </div>
                    <div class="col-sm-1 image">
                        <?php if ($product['status'] == 'Configurated') { ?>
                        <a data-pjax="0" style="margin: 20px;color: #397aa3;font-weight: 900;" class="text-center btn btn-link" href="<?= Url::to(['dedit', 'epid' => $product['id']]) ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                        <?php } else { ?>
                        <a href="<?=Url::to(['/shop-page/shop-page/product/','id'=>$product['id']])?>">
                            <?=Html::img($product['image'],['width'=> '100'])?>
                        </a>
                        <?php } ?>
                    </div>
                    <div class="col-sm-2 info product-title">
                        <a href="<?=Url::to(['/shop-page/shop-page/product/','id'=>$product['id']])?>">
                        <?=$product['title']?>
                        </a>
                    </div>
                    <div class="col-sm-1 info">
                        <?=Html::dropDownList('quantity',$product['Quantity'],QHelper::getQuantity(),[
                            'onchange' => "submitForm(this.form)"
                        ])?>
                    </div>
                    <div class="col-sm-5 info">

                        <?php if ($product['status'] == 'Configurated') { ?>
                        <?php if (!empty($product['productInfo'])) { ?>
                        <?php $data = unserialize($product['productInfo']);?>
                        <?php foreach ($data as $key => $val) { ?>
                        <?php if (empty($val)) continue; ?>
                        <div style="margin: 3px;"><b><?= Yii::t('app', $key) ?>:</b> <?= $val ?></div>
                        <?php } ?>
                        <?php } ?>
                        <?php } else { ?>
                        <div style="margin: 3px;"><b>Culoare:</b> <?=$product['productInfo']->doorColor->lang->Title?>   <span style="background-color: #8b9bb2!important; color: #fff; padding: 1px; margin: 1px;">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                        <div style="margin: 3px;"><b>Dimensiuni:</b> <?=$product['productInfo']->doorSize->WidthHeight?>   <span style="background-color: #8b9bb2!important; color: #fff; padding: 1px;margin: 1px;">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                        <div style="margin: 3px;"><b>Maner:</b> <?=$product['productInfo']->doorHandle->lang->Title?>   <span style="background-color: #8b9bb2!important; color: #fff; padding: 1px;margin: 1px;">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                        <?php
                        if (isset($product['productInfo']->material)) {
                            ?>
                            <div style="margin: 3px;"><b>Material:</b> <?= $product['productInfo']->material->lang->Title ?> <span
                                        style="background-color: #8b9bb2!important; color: #fff; padding: 1px;margin: 1px;">+ <?= $product['productInfo']->doorColor->Price ?>
                                    &euro;</span></div>
                            <?php
                        }
                        ?>
                        <?php } ?>
                        
                    </div>

                    <div class="col-sm-2 info">
                        <span>Unitate: </span><?=$product['price']?> &euro;<br>
                        <span>Total: </span><b><?=$product['subtotal']?> &euro; </b><br>
                    </div>
            </div>
                <?=Html::endForm()?>

            <?php
            }
            ?>

            <div class="row">
                <div class="col-sm-3">

                </div>
                <div class="col-sm-3">

                </div>
                <div class="col-sm-3"><b>Reducere la cantitate</b>
                    <?php
                    foreach ($discounts as $discount){
                        ?>
                        <div><b><?=$discount->Percent?>%</b> &nbsp; <span><?=$discount->Price?> &euro; </span></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-sm-3">
                    <div class="pull-right">

                        <div class="final-price">Pret total: <span class="sum"><?=$total?> &euro;</span></div>
                        <div class="final-price">Reducere: <span class="sum"><?=$percent?> % = <?=$discountTotal?> &euro;</span></div>
                        <div class="final-price">Pret cu reducere: <span class="sum"><?=$total-$discountTotal?> &euro;</span></div>


                    </div>
                </div>

            </div>
            <?php
            Pjax::end();
            ?>
        </div>
    </div>

    <div class="container ">
        <div class="pull-right">
            <a href="<?= Url::to(['/admin/ecommerce/order/order/create/']) ?>"
               class="btn-primary checkout-button" data-pjax="false">
                ACHITARE
            </a>
        </div>
    </div>
</div>
<?php $this->registerJs(
        "   function submitForm(form){
                var form = $(form);
                var formData = form.serialize();
                $.ajax({
                     url: form.attr('action'),
                     type: form.attr('method'),
                     data: formData,
                    success: function (data) {
                        $.pjax.reload({container: '#p1'});
             
                    },
                    error: function () {}
                });

            }
            
            function deleteThis(form){
                var form = $(form);
                var formData = form.serialize();
                $.ajax({
                     url: '/admin/ecommerce/order/cart/delete-item/',
                     type: form.attr('method'),
                     data: formData,
                    success: function (data) {
                        $.pjax.reload({container: '#p1'});   
                    },
                    error: function () {}
                });

            }
            "
        ,View::POS_HEAD);
?>
