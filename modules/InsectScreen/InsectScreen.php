<?php

namespace app\modules\InsectScreen;

/**
 * insect-screen module definition class
 */
class InsectScreen extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\InsectScreen\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
