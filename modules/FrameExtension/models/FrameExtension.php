<?php

namespace app\modules\FrameExtension\models;

use Yii;
use yii\caching\TagDependency;

/**
 * This is the model class for table "FrameExtension".
 *
 * @property integer $ID
 * @property integer $Size
 * @property integer $Left
 * @property integer $Rigth
 * @property integer $Top
 * @property integer $Bottom
 */
class FrameExtension extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FrameExtension';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Size'], 'required'],
            [['Size', 'Left', 'Rigth', 'Top', 'Bottom'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Size' => Yii::t('app', 'Size'),
            'Left' => Yii::t('app', 'Left'),
            'Rigth' => Yii::t('app', 'Rigth'),
            'Top' => Yii::t('app', 'Top'),
            'Bottom' => Yii::t('app', 'Bott'),
        ];
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
    }
    
}
