<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\FrameExtension\models\FrameExtension */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="frame-extension-form">

    <?php $form = ActiveForm::begin([
        'id' => 'sizes-form'
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            
            <?= $form->field($model, 'Size')->textInput() ?>
            
            <div class="row">
                <div class="col-md-12 text-center">
                    <?= $form->field($model, 'Left')->checkbox() ?>
                </div>
                <div class="col-md-4 text-center">
                    <?= $form->field($model, 'Rigth')->checkbox() ?>
                </div>
                <div class="col-md-4 text-center">
                    <?= Html::checkbox('All', $model->Left && $model->Rigth && $model->Top && $model->Bottom, [
                        'id' => 'toggle-ceckboxes'
                    ]) ?> <b>All</b>&nbsp;&nbsp;
                </div>
                <div class="col-md-4 text-center">
                    <?= $form->field($model, 'Top')->checkbox() ?>
                </div>
                <div class="col-md-12 text-center">
                    <?= $form->field($model, 'Bottom')->checkbox() ?>
                </div>
            </div>
            
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->registerJs("
    $('#toggle-ceckboxes').click(function(){
        var state = $(this).prop('checked');
        $('#sizes-form input[type=checkbox]').not(this).prop('checked', state);
    });
") ?>
