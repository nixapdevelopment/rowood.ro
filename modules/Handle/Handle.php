<?php

namespace app\modules\Handle;

/**
 * handle module definition class
 */
class Handle extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Handle\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
