<?php

namespace app\modules\Order\widgets\CartInfo;

use app\modules\Product\models\Product;
use yii\base\Widget;

class CartInfo extends Widget
{
   public function run()
   {    //unset($_SESSION['cart']);
       $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [] ;
        $products = [];
        $total = 0;
        foreach ($cart as $key => $item){
            $product = Product::find()->where(['ID'=>$key])->with(['lang'])->one();
            $subtotal = $item['Quantity'] * $product->totalPrice;
            $total += $subtotal;
            $products[] = [
                'Quantity' => $item['Quantity'],
                'title' => $product->lang->Title,
                'image' => isset($product->ParentID) ? $product->parentMainImage->imagePath  : $product->mainImage->imagePath,
                'price' => $product->totalPrice,
                'subtotal' => $subtotal,
                'id' => $key,

            ];
        }

       return $this->render('index',[
          'products' => $products,
           'total' => $total,
       ]);
   }
}