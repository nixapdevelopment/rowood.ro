<?php

namespace app\modules\Calculator\components;

use app\modules\Product\models\Product;
use app\modules\Material\models\Material;
use app\modules\Profile\models\Profile;
use app\modules\Color\models\Color;
use app\modules\Wood\models\Wood;
use yii\caching\TagDependency;

class CalculatorHelper
{
    
    public static function initNewProduct($productID, $materialID, $profileID, $frontColorID, $woodID = null, $backColorID = null)
    {
        $product = Product::findOne($productID);
        $configInfo = unserialize($product->ConfiguratorInfo);
        $configInfo = (array)$configInfo;
        
        $material = Material::getDb()->cache(function ($db) use ($materialID) {
            return Material::find()->with('lang')->where(['ID' => $materialID])->one();
        }, 3600, new TagDependency(['tags' => Material::className()]));
        
        $materialPrice = self::calculateMaterialPrice($material->Price, $product->Width, $product->Height);
        $configInfo['MaterialID'] = $material->lang->Title . ' ' . self::addPriceInfo($materialPrice);
        
        $profile = Profile::getDb()->cache(function ($db) use ($profileID) {
            return Profile::find()->with('lang')->where(['ID' => $profileID])->one();
        }, 3600, new TagDependency(['tags' => Profile::className()]));
        $configInfo['ProfileID'] = $profile->lang->Title . ' ' . self::addPriceInfo(self::calculateProfilePrice($materialPrice, $profile->Percent));
        
        $frontColor = Color::getDb()->cache(function ($db) use ($frontColorID) {
            return Color::find()->with('lang')->where(['ID' => $frontColorID])->one();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        $configInfo['FrontColorID'] = $material->lang->Title . ' ' . self::addPriceInfo(self::calculateColorPrice($materialPrice, $frontColor->Percent));
        
        if ($material->isMultiMaterial && $backColorID)
        {
            $backColor = Color::getDb()->cache(function ($db) use ($backColorID) {
                return Color::find()->with('lang')->where(['ID' => $backColorID])->one();
            }, 3600, new TagDependency(['tags' => Color::className()]));
            $configInfo['BackColorID'] = $material->lang->Title . ' ' . self::addPriceInfo(self::calculateColorPrice($materialPrice, $backColor->Percent));
        }
        else
        {
            unset($configInfo['BackColorID']);
        }
        
        if ($material->Wood == 1 && $woodID)
        {
            $wood = Wood::getDb()->cache(function ($db) use ($woodID) {
                return Wood::find()->with('lang')->where(['ID' => $woodID])->one();
            }, 3600, new TagDependency(['tags' => Wood::className()]));
            $configInfo['WoodID'] = $material->lang->Title . ' ' . self::addPriceInfo(self::calculateWoodPrice($materialPrice, $wood->Percent));
        }
        else
        {
            unset($configInfo['WoodID']);
        }
        
        $configInfo['Width'] = $product->Width . ' mm';
        $configInfo['Height'] = $product->Height . ' mm';
        
        $product->ConfiguratorInfo = serialize($configInfo);
        $product->save(false);
    }
    
    public static function calculateMaterialPrice($materialPrice, $productWidth, $productHeight)
    {
        return round($materialPrice * 2 * $productWidth * $productHeight / 1000000, 2);
    }
    
    public static function calculateProfilePrice($materialPriceWithDimentions, $profilePercent)
    {
        return round($materialPriceWithDimentions * $profilePercent / 100, 2);
    }
    
    public static function calculateColorPrice($materialPriceWithDimentions, $colorPercent)
    {
        return round($materialPriceWithDimentions * $colorPercent / 100, 2);
    }
    
    public static function calculateWoodPrice($materialPriceWithDimentions, $woodPercent)
    {
        return round($materialPriceWithDimentions * $woodPercent / 100, 2);
    }
    
    
    public static function calculateGlassPrice($product, $glass, $profileWidth)
    {
        return round($glass->GlassCount * ($product->Width - ($profileWidth * 2)) * ($product->Height - ($profileWidth * 2)) / 1000000 * $glass->Price, 2);
    }
    
    public static function calculateTermoEdgePrice($product, $glass, $termoEdge, $profileWidth)
    {
        return round((($product->Width - ($profileWidth * 2)) + ($product->Height - ($profileWidth * 2))) * 2 * ($glass->GlassCount - 1) / 1000 * $termoEdge->Price, 2);
    }
    
    public static function calculateSoundprofingPrice($product, $glass, $soundProofing, $profileWidth)
    {
        return round($glass->GlassCount * ($product->Width - ($profileWidth * 2)) * ($product->Height - ($profileWidth * 2)) / 1000000 * $soundProofing->Price, 2);
    }
    
    public static function calculateSafetyPrice($product, $glass, $safety, $profileWidth)
    {
        return round($glass->GlassCount * ($product->Width - ($profileWidth * 2)) * ($product->Height - ($profileWidth * 2)) / 1000000 * $safety->Price, 2);
    }
    
    public static function calculateDecorationPrice($product, $glass, $decoration, $profileWidth)
    {
        return round($glass->GlassCount * ($product->Width - ($profileWidth * 2)) * ($product->Height - ($profileWidth * 2)) / 1000000 * $decoration->Price, 2);
    }
    
    public static function addPriceInfo($price)
    {
        return $price > 0 ? '<span class="label label-default">+ ' . number_format($price, 2) . ' &euro;</span>' : '';
    }
    
}
