<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel">
    <div class="form-panel">
        <div class="calculator">
            <div class="row">
                <div class="col-md-9 col-md-12">
                    <div class="row">
                        <div class="col-md-6 remove-padding-right">
                            <div class="top-panel">
                                <div class="title-panel">
                                    DIMENSIUNEA
                                </div>
                                <div class="info">
                                    Vă rugăm să introduceți dimensiunea dorită incl. Toate atașamentele în milimetri
                                </div>
                            </div>
                            <div class="panel-content-padding">
                                <div class="input-group">
                                    <span class="input-group-addon">Lățimea totală:</span>
                                    <input onchange="Calculator.setDimensions()" type="text" class="form-control" placeholder="00" data-min="<?php if(isset($variant->MinWidth)){echo $variant->MinWidth;}  ?>" data-max="<?php if(isset($variant->MaxWidth)){echo $variant->MaxWidth;}  ?>" name="FullWidth" value="<?= $product->Width ?>" required="required">
                                    <span class="input-group-addon">mm</span>
                                </div>
                                <div class="input-group-text">
                                    Gama admisă: <?= $variant->MinWidth ?> mm - <?= $variant->MaxWidth ?> mm
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">Înălțime totală:</span>
                                    <input onchange="Calculator.setDimensions()" type="text" class="form-control" placeholder="00" data-min="<?= $variant->MinHeight ?>" data-max="<?= $variant->MaxHeight ?>" name="FullHeight" value="<?= $product->Height ?>" required="required">
                                    <span class="input-group-addon">mm</span>
                                </div>
                                <div class="input-group-text">
                                    Gama admisă: <?= $variant->MinHeight ?> mm - <?= $variant->MaxHeight ?> mm
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 remove-padding">
                            <?= $this->render('part-sizes', [
                                'type' => $type,
                                'product' => $product,
                                'rightDimensions' => $rightDimensions,
                                'bottomDimensions' => $bottomDimensions,
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div id="product-info-wrap" class="col-md-3 remove-padding-left">
                    <?= $this->render('product-info', [
                        'product' => $product,
                        'material' => $material,
                        'profile' => $profile,
                        'wood' => $wood,
                        'frontColor' => $frontColor,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>