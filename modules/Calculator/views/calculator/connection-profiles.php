<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'CONNECTION PROFILES') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>
<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        <?= Yii::t('app', 'Connection profiles') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setConnectionProfile(this.value)" <?= !$product->ConnectionProfile ? 'checked' : '' ?> style="display: block" type="radio" name="ConnectionProfile" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setConnectionProfile(this.value)" <?= $product->ConnectionProfile ? 'checked' : '' ?> style="display: block" type="radio" name="ConnectionProfile" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div <?= !$product->ConnectionProfile ? 'style="display:none;"' : '' ?> class="form-panel-content connection-profile-variants">
    <div>
        <div class="row">
            <?php foreach ($connectionProfiles as $connectionProfile) { ?>
            <div class="col-md-12">
                <label for="connection-profile-<?= $connectionProfile->ID ?>">
                    <input style="display: inline-block;" onchange="Calculator.setConnectionProfileID(<?= $connectionProfile->ID ?>, <?= isset($pConnectionProfiles[$connectionProfile->ID]) ? (float)$pConnectionProfiles[$connectionProfile->ID] : 0 ?>)" <?= $product->ConnectionProfileID == $connectionProfile->ID ? 'checked' : '' ?> type="radio" name="ConnectionProfileID" value="<?= $connectionProfile->ID ?>" id="connection-profile-<?= $connectionProfile->ID ?>"> 
                    <?= $connectionProfile->lang->Title ?> 
                    <?php if (isset($pConnectionProfiles[$connectionProfile->ID])) { ?>
                        <span class="label label-default">+ <?= $pConnectionProfiles[$connectionProfile->ID] ?> &euro;</span>
                    <?php } ?>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>