<?php

namespace app\modules\Calculator\controllers;

use Yii;
use yii\web\Response;
use yii\caching\TagDependency;
use app\modules\Profile\models\Profile;
use app\components\Controller\FrontController;
use app\components\PriceConverter\PriceConverter;
use app\modules\Product\models\Product;
use app\modules\Material\models\Material;
use app\modules\Wood\models\Wood;
use app\modules\Color\models\Color;
use app\modules\Type\models\Type;
use app\modules\Type\models\Variant;
use app\modules\Glass\models\Glass;
use app\modules\TermoEdge\models\TermoEdge;
use app\modules\Opening\models\Opening;
use app\modules\Soundproofing\models\Soundproofing;
use app\modules\Safety\models\Safety;
use app\modules\Decoration\models\Decoration;
use app\modules\Calculator\components\CalculatorHelper;
use app\modules\Windowsill\models\WindowsillColor;

class AjaxController extends FrontController
{
    
    public $ProfileWidth = 110;
    
    public function init()
    {
        parent::init();
        
        if (!Yii::$app->request->isAjax)
        {
            exit();
        }
        
        Yii::$container->set('yii\widgets\Pjax', [
            'timeout' => 5000,
        ]);
        
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionGetProfiles($productID, $materialID)
    {
        $product = $this->updateProduct($productID, 'MaterialID', $materialID);
        
        $material = Material::getDb()->cache(function ($db) use ($materialID) {
            return Material::find()->with('lang')->where(['ID' => $materialID])->one();
        }, 3600, new TagDependency(['tags' => Material::className()]));
        
        $profiles = Profile::getDb()->cache(function ($db) use ($materialID) {
            return Profile::find()->with('lang')->where(['MaterialID' => $materialID])->all();
        }, 3600, new TagDependency(['tags' => Profile::className()]));
        
        $woods = Wood::getDb()->cache(function ($db) {
            return Wood::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Wood::className()]));
        
        $frontColors = Color::getDb()->cache(function ($db) use ($product, $materialID, $profileID, $woodID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->andFilterWhere(['MaterialID' => $materialID], ['ProfileID' => $profileID], ['WoodID' => $woodID], ['Front' => 1])
                    ->orderBy('Position')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
         
        $defaultProfile = reset($profiles);
        
        foreach ($frontColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == !empty($defaultProfile) ? $defaultProfile->ID : -1)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($frontColors[$key]);
                    continue;
                }
            }
        }
        
        $backColors = Color::getDb()->cache(function ($db) use ($product, $materialID, $profileID, $woodID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->andFilterWhere(['MaterialID' => $materialID], ['ProfileID' => $profileID], ['WoodID' => $woodID], ['Back' => 1])
                    ->orderBy('Position')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        
        foreach ($backColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == !empty($defaultProfile) ? $defaultProfile->ID : -1)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($backColors[$key]);
                    continue;
                }
            }
        }

        return [
            'profilesContent' => $this->renderPartial('../calculator/profiles', [
                'profiles' => $profiles,
                'product' => $product,
            ]),
            'woodsContent' => $material->Wood ? $this->renderPartial('../calculator/woods', [
                'woods' => $woods,
                'product' => $product,
            ]) : null,
            'frontColorsContent' => $this->renderPartial('../calculator/front-colors', [
                'frontColors' => $frontColors,
                'product' => $product,
            ]),
            'backColorsContent' => $this->renderPartial('../calculator/back-colors', [
                'backColors' => $backColors,
                'product' => $product,
            ]),
            'total' => PriceConverter::convert(0),
        ];
    }
    
    public function actionSetProfiles($productID, $profileID)
    {
        $product = $this->updateProduct($productID, 'ProfileID', $profileID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWood($productID, $woodID)
    {
        $product = $this->updateProduct($productID, 'WoodID', $woodID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionGetColors($productID, $materialID, $profileID, $woodID = null)
    {
        $product = $this->updateProduct($productID, 'WoodID', $woodID);
        
        $frontColors = Color::getDb()->cache(function ($db) use ($product, $materialID, $profileID, $woodID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->andFilterWhere(['MaterialID' => $materialID], ['ProfileID' => $profileID], ['WoodID' => $woodID], ['Front' => 1])
                    ->orderBy('Position')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
                    
        foreach ($frontColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == $profileID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($frontColors[$key]);
                    continue;
                }
            }
        }
        
        $backColors = Color::getDb()->cache(function ($db) use ($product, $materialID, $profileID, $woodID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->andFilterWhere(['MaterialID' => $materialID], ['ProfileID' => $profileID], ['WoodID' => $woodID], ['Back' => 1])
                    ->orderBy('Position')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        
        foreach ($backColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == $profileID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($backColors[$key]);
                    continue;
                }
            }
        }

        return [
            'frontColorsContent' => $this->renderPartial('../calculator/front-colors', [
                'frontColors' => $frontColors,
                'product' => $product,
            ]),
            'backColorsContent' => $this->renderPartial('../calculator/back-colors', [
                'backColors' => $backColors,
                'product' => $product,
            ]),
            'total' => PriceConverter::convert(0),
        ];
    }
    
    public function actionSetColor($productID, $colorID, $side)
    {
        $attribute = $side == 'front' ? 'FrontColorID' : 'BackColorID';
        
        $product = $this->updateProduct($productID, $attribute, $colorID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetType($productID, $typeID, $saveSizes = 0)
    {
        $product = $this->updateProduct($productID, 'TypeID', $typeID);
        
        if (!$saveSizes)
        {
            Product::updateAll(['Width' => null, 'Height' => null], ['ID' => $productID]);
        }
        
        $variants = Variant::find()->where(['TypeID' => $product->TypeID])->orderBy('Position')->all();
        
        return [
            'variantsContent' => $this->renderPartial('../calculator/variants', [
                'variants' => $variants,
                'product' => $product,
            ]),
            'product' => $product,
        ];
    }
    
    public function actionSetVariant($productID, $variantID, $saveSizes = 0)
    {
        $product = $this->updateProduct($productID, 'VariantID', $variantID);
        
        if (!$saveSizes)
        {
            Product::updateAll(['Width' => null, 'Height' => null, 'Price' => null], ['ID' => $productID]);
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionGetSizes($productID)
    {
        $product = Product::findOne($productID);
        
        $material = Material::findOne($product->MaterialID);
        $profile = Profile::findOne($product->ProfileID);
        $frontColor = Color::findOne($product->FrontColorID);
        
        $type = Type::findOne($product->TypeID);
        
        $variant = Variant::findOne($product->VariantID);
        
        $rightDimensions = [];
        $stack = 0;
        for ($i = 0; $i < $type->RightDimensions; $i++)
        {
            if ($i == $type->RightDimensions - 1)
            {
                $rightDimensions[] = round($product->Height - $stack);
            }
            else
            {
                $res = round($product->Height / $type->RightDimensions);
                $rightDimensions[] = $res;
                $stack += $res;
            }
        }
        
        $product = $this->updateProduct($productID, 'RightDimensions', serialize($rightDimensions));
        
        $bottomDimensions = [];
        $stack = 0;
        for ($i = 0; $i < $type->BottomDimensions; $i++)
        {
            if ($i == $type->BottomDimensions - 1)
            {
                $bottomDimensions[$i] = round($product->Width - $stack);
            }
            else
            {
                $res = round($product->Width / $type->BottomDimensions);
                $bottomDimensions[$i] = $res;
                $stack += $res;
            }
        }
        
        $product = $this->updateProduct($productID, 'BottomDimensions', serialize($bottomDimensions));
        
        return [
            'sizesContent' => $this->renderPartial('../calculator/sizes', [
                'product' => $product,
                'type' => $type,
                'variant' => $variant,
                'material' => $material,
                'profile' => $profile,
                'frontColor' => $frontColor,
                'rightDimensions' => $rightDimensions,
                'bottomDimensions' => $bottomDimensions,
            ]),
            'product' => $product,
        ];
    }
    
    public function actionSetDimensions($productID, $width, $height)
    {
        $width = (int)$width;
        $height = (int)$height;
        
        $product = $this->updateProduct($productID, 'Width', $width);
        $product = $this->updateProduct($productID, 'Height', $height);
        
        CalculatorHelper::initNewProduct($product->ID, $product->MaterialID, $product->ProfileID, $product->FrontColorID, $product->WoodID, $product->BackColorID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionGetPrice($productID)
    {
        $product = Product::findOne($productID);
        
        $material = Material::findOne($product->MaterialID);
        
        // init price
        $price = $material->Price;
        
        if (!$product->Width || !$product->Height)
        {
            return [
                'price' => 0,
            ];
        }
        
        // price with dimensions
        $priceWithDimensions = CalculatorHelper::calculateMaterialPrice($price, $product->Width, $product->Height);
        $totalPrice = $priceWithDimensions;
        
        // profile
        $profile = Profile::findOne($product->ProfileID);
        if ($profile->Percent > 0)
        {
            $totalPrice += '';
        }
        
        // wood
        if ($material->Wood && $product->WoodID)
        {
            $wood = Wood::findOne($product->WoodID);
            
            if ($wood->Percent > 0)
            {
                $totalPrice += round($priceWithDimensions * $wood->Percent / 100, 2);
            }
        }
        
        // front color
        $frontColor = Color::findOne($product->FrontColorID);
        if ($frontColor->Percent > 0)
        {
            $totalPrice += round($priceWithDimensions * $frontColor->Percent / 100, 2);
        }
        
        // back color
        if ($material->isMultiMaterial && $product->BackColorID)
        {
            $backColor = Color::findOne($product->BackColorID);
            if ($backColor->Percent > 0)
            {
                $totalPrice += round($priceWithDimensions * $backColor->Percent / 100, 2);
            }
        }
        
        // variant
        $variant = Variant::findOne($product->VariantID);
        if ($variant->Number > 0)
        {
            $opening = Opening::find()->where(['>', 'From', $product->Height])->andWhere(['<=', 'To', $product->Height])->one();
            
            $totalPrice += $variant->Number * (isset($opening->Price) ? $opening->Price : 50);
        }
        
        // glass
        if ($product->GlassID)
        {
            $glass = Glass::findOne($product->GlassID);
            
            $totalPrice += round($glass->GlassCount * ($product->Width - ($this->ProfileWidth * 2)) * ($product->Height - ($this->ProfileWidth * 2)) / 1000000 * $glass->Price, 2);
        }
        
        // termo edge
        if ($product->GlassID && $product->TermoEdge && $product->TermoEdgeID)
        {
            $termoEdge = TermoEdge::findOne($product->TermoEdgeID);
            
            $totalPrice += round((($product->Width - ($this->ProfileWidth * 2)) + ($product->Height - ($this->ProfileWidth * 2))) * 2 * $glass->GlassCount / 1000 * $termoEdge->Price, 2);
        }
        
        // sound proofing
        if ($product->SoundProofing && $product->SoundProofingID)
        {
            $soundProofing = Soundproofing::findOne($product->SoundProofingID);
            
            $totalPrice += round($glass->GlassCount * ($product->Width - ($this->ProfileWidth * 2)) * ($product->Height - ($this->ProfileWidth * 2)) / 1000000 * $soundProofing->Price, 2);
        }
        
        // safety
        if ($product->Safety && $product->SafetyID)
        {
            $safety = Safety::findOne($product->SafetyID);
            
            $totalPrice += round($glass->GlassCount * ($product->Width - ($this->ProfileWidth * 2)) * ($product->Height - ($this->ProfileWidth * 2)) / 1000000 * $safety->Price, 2);
        }
        
        // decoration
        if ($product->Decoration && $product->DecorationID)
        {
            $decoration = Decoration::findOne($product->DecorationID);
            
            $totalPrice += round($glass->GlassCount * ($product->Width - ($this->ProfileWidth * 2)) * ($product->Height - ($this->ProfileWidth * 2)) / 1000000 * $decoration->Price, 2);
        }
        
        if ($product->MontageHoles)
        {
            $totalPrice += 15.04;
        }
        
        if ($product->Windowsill && $product->WindowsillLength && $product->WindowsillWidth)
        {
            $totalPrice += ($product->WindowsillLength * $product->WindowsillWidth) / 1000000 * 35.6;
        }
        
        if ($product->Anschraubdichtung)
        {
            $totalPrice += 43.72;
        }
        
        $totalPrice += round($totalPrice * 19 / 100, 2);
        
        $product = $this->updateProduct($productID, 'Price', $totalPrice);
        $product = $this->updateProduct($productID, 'Quantity', 1);
        
        return [
            'productInfoContent' => $this->renderPartial('../calculator/product-info', [
                'product' => $product,
                'material' => $material,
                'profile' => $profile,
                'wood' => $wood,
                'frontColor' => $frontColor,
            ]),
            'product' => $product,
        ];
    }
    
    public function actionGetGlasses($profileID)
    {
        $profile = Profile::findOne($profileID);
        
        $glasses = Glass::find()->with(['lang'])->where(['<=', 'GlassCount', $profile->MaxGlassCount])->indexBy('ID')->all();
        
        return [
            'glassesContent' => $this->renderPartial('../calculator/glasses', [
                'glasses' => $glasses
            ]),
        ];
    }

    public function actionSetGlass($productID, $glassID)
    {
        $product = $this->updateProduct($productID, 'GlassID', $glassID);
        
        $base = Glass::findOne(['Default' => 1]);
        $new = Glass::findOne($glassID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $data['GlassID'] = $new->lang->Title . ' ' . CalculatorHelper::addPriceInfo((CalculatorHelper::calculateGlassPrice($product, $new, 110)));
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetTermoEdge($productID, $termoEdge)
    {
        $termoEdge = (int)$termoEdge;
        
        $product = $this->updateProduct($productID, 'TermoEdge', $termoEdge);
        
        if ($termoEdge == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['TermoEdgeID']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetTermoEdgeId($productID, $termoEdgeID)
    {
        $product = $this->updateProduct($productID, 'TermoEdgeID', $termoEdgeID);
        
        $termoEdge = TermoEdge::find()->joinWith('lang')->where(['TermoEdge.ID' => $termoEdgeID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['TermoEdgeID'] = $termoEdge->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateTermoEdgePrice($product, $glass, $termoEdge, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSoundProofing($productID, $soundProofing)
    {
        $soundProofing = (int)$soundProofing;
        
        $product = $this->updateProduct($productID, 'SoundProofing', $soundProofing);
        
        if ($soundProofing == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['SoundProofingID']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSoundProofingId($productID, $soundProofingID)
    {
        $product = $this->updateProduct($productID, 'SoundProofingID', $soundProofingID);
        
        $soundProofing = Soundproofing::find()->joinWith('lang')->where(['Soundproofing.ID' => $soundProofingID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['SoundProofingID'] = $soundProofing->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateSoundprofingPrice($product, $glass, $soundProofing, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSafety($productID, $safety)
    {
        $safety = (int)$safety;
        
        $product = $this->updateProduct($productID, 'Safety', $safety);
        
        if ($safety == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['SafetyID']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSafetyId($productID, $safetyID)
    {
        $product = $this->updateProduct($productID, 'SafetyID', $safetyID);
        
        $safety = Safety::find()->joinWith('lang')->where(['Safety.ID' => $safetyID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['SafetyID'] = $safety->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateSafetyPrice($product, $glass, $safety, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetDecoration($productID, $decoration)
    {
        $decoration = (int)$decoration;
        
        $product = $this->updateProduct($productID, 'Decoration', $decoration);
        
        if ($decoration == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['Decoration']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetDecorationId($productID, $decorationID)
    {
        $product = $this->updateProduct($productID, 'DecorationID', $decorationID);
        
        $decoration = Decoration::find()->joinWith('lang')->where(['Decoration.ID' => $decorationID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['DecorationID'] = $decoration->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateDecorationPrice($product, $glass, $decoration, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetMontageHoles($productID, $montageHoles)
    {
        $montageHoles = (int)$montageHoles;
        
        $product = $this->updateProduct($productID, 'MontageHoles', $montageHoles);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($montageHoles == 0)
        {
            unset($data['MontageHoles']);
        }
        else
        {
            $data['MontageHoles'] = 'Da <span class="label label-default">+ 15.04 €</span>';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsill($productID, $windowsill)
    {
        $windowsill = (int)$windowsill;
        
        $product = $this->updateProduct($productID, 'Windowsill', $windowsill);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($windowsill == 0)
        {
            unset($data['Windowsill']);
        }
        else
        {
            $data['Windowsill'] = 'Da';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsillLength($productID, $length)
    {
        $length = (int)$length;
        
        $product = $this->updateProduct($productID, 'WindowsillLength', $length);
        
        $data = unserialize($product->ConfiguratorInfo);
        $data['WindowsillLength'] = $length;
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsillColor($productID, $colorID)
    {
        $colorID = (int)$colorID;
        
        $product = $this->updateProduct($productID, 'WindowsillColorID', $colorID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $color = WindowsillColor::find()->joinWith('lang')->where(['WindowsillColor.ID' => $colorID])->one();
        $data['WindowsillColorID'] = $color->lang->Title;
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsillWidth($productID, $width)
    {
        $colorID = (int)$colorID;
        
        $product = $this->updateProduct($productID, 'WindowsillWidth', $width);
        
        $data = unserialize($product->ConfiguratorInfo);
        $data['WindowsillWidth'] = $product->WindowsillWidth . ' mm <span class="label label-default">+ ' . number_format(($product->WindowsillLength * $width) / 1000000 * 35.6, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetEndCorner($productID, $endCorner)
    {
        $endCorner = (int)$endCorner;
        
        $product = $this->updateProduct($productID, 'EndCorner', $endCorner);
        
        if ($endCorner == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['EndCorner']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetEndCornerId($productID, $endCornerID)
    {
        $endCorner = (int)$endCorner;
        
        $product = $this->updateProduct($productID, 'EndCornerID', $endCornerID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $endCorner = \app\modules\Windowsill\models\EndCorner::find()->joinWith('lang')->where(['EndCorner.ID' => $endCornerID])->one();
        $data['EndCornerID'] = $endCorner->lang->Title . ' <span class="label label-default">+ ' . number_format($endCorner->Price * 2, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetHConnector($productID, $HConnector)
    {
        $HConnector = (int)$HConnector;
        
        $product = $this->updateProduct($productID, 'HConnector', $HConnector);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($HConnector == 0)
        {
            unset($data['HConnector']);
        }
        else
        {
            $data['HConnector'] = 'Da';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetScrew($productID, $screw)
    {
        $screw = (int)$screw;
        
        $product = $this->updateProduct($productID, 'Screw', $screw);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($screw == 0)
        {
            unset($data['Screw']);
        }
        else
        {
            $data['Screw'] = 'Da';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetScrewId($productID, $screwID)
    {
        $screwID = (int)$screwID;
        
        $product = $this->updateProduct($productID, 'ScrewID', $screwID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $screw = \app\modules\Windowsill\models\Screw::find()->joinWith('lang')->where(['Screw.ID' => $screwID])->one();
        $data['ScrewID'] = $screw->lang->Title . ' <span class="label label-default">+ ' . number_format($screw->Price * ceil($product->Width / 300), 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetFittingId($productID, $fittingID)
    {
        $fittingID = (int)$fittingID;
        
        $product = $this->updateProduct($productID, 'FittingID', $fittingID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $fitting = \app\modules\Fitting\models\Fitting::find()->joinWith('lang')->where(['Fitting.ID' => $fittingID])->one();
        $variant = Variant::find()->where(['ID' => $product->VariantID])->one();
        $data['FittingID'] = $fitting->lang->Title . ' <span class="label label-default">+ ' . number_format($fitting->Price * $variant->Number, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetAnschraubdichtung($productID, $anschraubdichtung)
    {
        $anschraubdichtung = (int)$anschraubdichtung;
        
        $product = $this->updateProduct($productID, 'Anschraubdichtung', $anschraubdichtung);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($anschraubdichtung == 0)
        {
            unset($data['Anschraubdichtung']);
        }
        else
        {
            $data['Anschraubdichtung'] = 'Da';
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetAnschraubdichtungId($productID, $anschraubdichtungID)
    {
        $product = $this->updateProduct($productID, 'AnschraubdichtungID', $anschraubdichtungID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $anschraubdichtung = \app\modules\Windowsill\models\Anschraubdichtung::find()->joinWith('lang')->where(['Anschraubdichtung.ID' => $anschraubdichtungID])->one();
        $data['AnschraubdichtungID'] = $anschraubdichtung->lang->Title . ' <span class="label label-default">+ ' . number_format($product->WindowsillWidth * 17.05 / 1000, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSoundLine($productID, $soundLine)
    {
        $soundLine = (int)$soundLine;
        
        $product = $this->updateProduct($productID, 'SoundLine', $soundLine);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($soundLine)
        {
            $data['SoundLine'] = 'Da <span class="label label-default">+ ' . number_format($product->WindowsillWidth * 15.05 / 1000, 2) . ' €</span>';
        }
        else
        {
            unset($data['SoundLine']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetInsectScreen($productID, $insectScreen)
    {
        $insectScreen = (int)$insectScreen;
        
        $product = $this->updateProduct($productID, 'InsectScreen', $insectScreen);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($insectScreen)
        {
            $data['InsectScreen'] = 'Da';
        }
        else
        {
            unset($data['InsectScreen']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetInsectScreenId($productID, $insectScreenID)
    {
        $product = $this->updateProduct($productID, 'InsectScreenID', $insectScreenID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $insectScreen = \app\modules\InsectScreen\models\InsectScreenColor::find()->joinWith('lang')->where(['InsectScreenColor.ID' => $insectScreenID])->one();
        $data['InsectScreenID'] = $insectScreen->lang->Title . ' <span class="label label-default">+ ' . number_format(($product->Width + $product->Height) * 2 / 1000 * $insectScreen->Price, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetClothId($productID, $clothID)
    {
        $product = $this->updateProduct($productID, 'ClothID', $clothID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $cloth = \app\modules\InsectScreen\models\Cloth::find()->joinWith('lang')->where(['Cloth.ID' => $clothID])->one();
        $data['ClothID'] = $cloth->lang->Title . ' <span class="label label-default">+ ' . number_format(($product->Width + $product->Height) * 2 / 1000 * $cloth->Price, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetFrameExtension($productID, $part, $size, $addPrice = 0)
    {
        $size = (int)$size;
        
        switch ($part)
        {
            case 'Left':
                $attribute = 'FrameExtensionLeft';
                break;
            case 'Right':
                $attribute = 'FrameExtensionRight';
                break;
            case 'Top':
                $attribute = 'FrameExtensionTop';
                break;
            case 'Bottom':
                $attribute = 'FrameExtensionBottom';
                break;
        }
        
        $product = $this->updateProduct($productID, $attribute, $size);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($addPrice > 0)
        {
            $data[$attribute] = $size . ' mm <span class="label label-default">+ ' . $addPrice . ' €</span>';
        }
        else
        {
            unset($data[$attribute]);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossen($productID, $sprossen)
    {
        $sprossen = (int)$sprossen;
        
        $product = $this->updateProduct($productID, 'Sprossen', $sprossen);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($sprossen)
        {
            $data['Sprossen'] = 'Da';
        }
        else
        {
            unset($data['Sprossen']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossenId($productID, $sprossenID)
    {
        $product = $this->updateProduct($productID, 'SprossenID', $sprossenID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossenWidth($productID, $sprossenWidth)
    {
        $product = $this->updateProduct($productID, 'SprossenWidth', $sprossenWidth);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetHandle($productID, $handle)
    {
        $handle = (int)$handle;
        
        $product = $this->updateProduct($productID, 'Handle', $handle);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($handle)
        {
            $data['Handle'] = 'Da';
        }
        else
        {
            unset($data['Handle']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetHandleId($productID, $handleID, $addPrice = 0)
    {
        $product = $this->updateProduct($productID, 'HandleID', $handleID);
        
        $handle = \app\modules\Handle\models\Handle::find()->with('lang')->where(['ID' => $handleID])->one();
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($addPrice > 0)
        {
            $data['HandleID'] = $handle->lang->Title . ' <span class="label label-default">+ ' . number_format($addPrice, 2) . ' €</span>';
        }
        else
        {
            unset($data['Handle']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetAirRegulator($productID, $airRegulator)
    {
        $airRegulator = (int)$airRegulator;
        
        $product = $this->updateProduct($productID, 'AirRegulator', $airRegulator);
        $variant = Variant::findOne($product->VariantID);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($airRegulator)
        {
            $data['AirRegulator'] = 'Da <span class="label label-default">+ ' . number_format(14.20 * $variant->Number, 2) . ' €</span>';
        }
        else
        {
            $data['AirRegulator'] = 'Nu';
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetConnectionProfile($productID, $connectionProfile)
    {
        $connectionProfile = (int)$connectionProfile;
        
        $product = $this->updateProduct($productID, 'ConnectionProfile', $connectionProfile);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($connectionProfile)
        {
            $data['ConnectionProfile'] = 'Da';
        }
        else
        {
            $data['ConnectionProfile'] = 'Nu';
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetConnectionProfileId($productID, $connectionProfileID, $addPrice = 0)
    {
        $product = $this->updateProduct($productID, 'ConnectionProfileID', $connectionProfileID);
        
        $connectionProfile = \app\modules\Windowsill\models\ConnectionProfile::find()->with('lang')->where(['ID' => $connectionProfileID])->one();
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($addPrice > 0)
        {
            $data['ConnectionProfile'] = $connectionProfile->lang->Title . ' <span class="label label-default">+ ' . number_format($product->WindowsillWidth * $connectionProfile->Price / 1000, 2) . ' €</span>';
        }
        else
        {
            unset($data['ConnectionProfile']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousies($productID, $jalousies)
    {
        $jalousies = (int)$jalousies;
        
        $product = $this->updateProduct($productID, 'Jalousies', $jalousies);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($connectionProfile)
        {
            $data['Jalousies'] = 'Da';
        }
        else
        {
            $data['Jalousies'] = 'Nu';
            unset($data['JalousieID']);
            unset($data['JalousieModelID']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieId($productID, $jalousieID)
    {
        $product = $this->updateProduct($productID, 'JalousieID', $jalousieID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $jalousie = \app\modules\Jalousie\models\Jalousie::find()->with('lang')->where(['Jalousie.ID' => $jalousieID])->one();
        $data['JalousieID'] = $jalousie->lang->Title;
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieModelId($productID, $jalousieModelID, $addPrice = 0)
    {
        $product = $this->updateProduct($productID, 'JalousieModelID', $jalousieModelID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $jalousieModel = \app\modules\Jalousie\models\Model::find()->with('lang')->where(['Model.ID' => $jalousieModelID])->one();
        $data['JalousieModelID'] = $jalousieModel->lang->Title . ' <span class="label label-default">+ ' . number_format($addPrice, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieModelSizeId($productID, $jalousieModelSizeID)
    {
        $product = $this->updateProduct($productID, 'JalousieModelSizeID', $jalousieModelSizeID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $jalousieModelSize = \app\modules\Jalousie\models\ModelSize::find()->with('lang')->where(['ModelSize.ID' => $jalousieModelSizeID])->one();
        $data['JalousieModelSizeID'] = $jalousieModelSize->lang->Title;
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieModelColorId($productID, $jalousieModelColorID)
    {
        $product = $this->updateProduct($productID, 'JalousieModelColorID', $jalousieModelColorID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $jalousieModelColor = \app\modules\Jalousie\models\ModelColor::find()->with('lang')->where(['ModelColor.ID' => $jalousieModelColorID])->one();
        $data['JalousieModelColorID'] = $jalousieModelColor->lang->Title  . ' <span class="label label-default">+ ' . number_format($jalousieModelColor->Price * $product->Width * $product->Height / 1000000, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieDimensions($productID, $width, $height)
    {
        $width = (int)$width;
        $height = (int)$height;
        
        $product = $this->updateProduct($productID, 'JalousieWidth', $width);
        $product = $this->updateProduct($productID, 'JalousieHeight', $height);
        
        $data = unserialize($product->ConfiguratorInfo);
        $data['JalousieWidth'] = $width . ' mm';
        $data['JalousieHeight'] = $height . ' mm';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossenCount()
    {
        $productID = \Yii::$app->request->post('productID');
        $data = \Yii::$app->request->post('data');
        parse_str($data, $data);
        
        $product = $this->updateProduct($productID, 'SprossenCount', serialize($data));
        
        return [
            'product' => $product,
            'data' => $data,
        ];
    }

    protected function updateProduct($productID, $attribute, $value)
    {
        $product = Product::findOne($productID);
        $product->$attribute = $value;
        $product->save(false);
        return $product;
    }

}
