<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'ANTIDRÖHNSTREIFEN') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>
<div class="form-panel-content">
    <div>
        <?= Yii::t('app', 'ANTIDRÖHNSTREIFEN') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSoundLine(this.value)" <?= !$product->SoundLine ? 'checked' : '' ?> style="display: block" type="radio" name="SoundLine" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSoundLine(this.value)" <?= $product->SoundLine ? 'checked' : '' ?> style="display: block" type="radio" name="SoundLine" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>