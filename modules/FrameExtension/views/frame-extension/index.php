<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\FrameExtension\models\FrameExtensionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Frame Extensions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frame-extension-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Frame Extension'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Size',
                    'filter' => Html::activeInput('number', $searchModel, 'Size', ['class' => 'form-control']),
                ],
                [
                    'attribute' => 'Left',
                    'filter' => false,
                    'value' => function($model)
                    {
                        return $model->Left ? 'Da' : 'Nu';
                    }
                ],
                [
                    'attribute' => 'Rigth',
                    'filter' => false,
                    'value' => function($model)
                    {
                        return $model->Rigth ? 'Da' : 'Nu';
                    }
                ],
                [
                    'attribute' => 'Top',
                    'filter' => false,
                    'value' => function($model)
                    {
                        return $model->Top ? 'Da' : 'Nu';
                    }
                ],
                [
                    'attribute' => 'Bottom',
                    'filter' => false,
                    'value' => function($model)
                    {
                        return $model->Bottom ? 'Da' : 'Nu';
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => [
                        'width' => 80,
                    ],
                    'template' => '<div class="text-center">{update}&nbsp;&nbsp;{delete}</div>'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>