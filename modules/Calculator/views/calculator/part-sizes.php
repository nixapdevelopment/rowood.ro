<?php

    use yii\bootstrap\Html;

?>


<div class="top-panel">
    <div class="title-panel">
        CLASIFICAREA
    </div>
    <div class="info">
        Vă rugăm să luați dispunerea dorită a ferestrei înainte
    </div>
</div>
<div class="panel-content-padding">
    <div class="row">
        <div class="col-md-8">
            <div class="calculator-img">
                <?= Html::img($type->imagePath) ?>
            </div>
            <?php if ($type->BottomDimensions) { ?>
            <?php for ($i = 0; $i < $type->BottomDimensions; $i++) { ?>
            <div style="display: inline-block; width: <?= round(100/$type->BottomDimensions) - 2 ?>%;" class="size-glass mt10">
                <label>
                    <input onchange="Calculator.recalculateSizes(this, <?= $product->Width ?>)" <?= $i > 1 ? 'readonly' : '' ?> type="text" placeholder="00" required="required" value="<?= $bottomDimensions[$i] ?>" name="BottomDimensions[]">
                </label>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="col-md-4">
            <div class="size-glass">
                <?php if ($type->RightDimensions) { ?>
                <?php for ($i = 0; $i < $type->RightDimensions; $i++) { ?>
                <div class="top-glass mt30">
                    <label>
                        <input onchange="Calculator.recalculateSizes(this, <?= $product->Height ?>)" type="text" placeholder="00" required="required" value="<?= $rightDimensions[$i] ?>" name="LeftDimensions[]">
                    </label>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>