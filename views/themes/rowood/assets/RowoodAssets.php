<?php

namespace app\views\themes\rowood\assets;

use yii\web\AssetBundle;

class RowoodAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/rowood/assets/files';
    
    public $css = [
        'js/owl-slider/dist/assets/owl.carousel.css',
        'js/swiper-slider/css/swiper.css',
        'css/main.css',
        
    ];
    
    public $js = [
        'js/owl-slider/dist/owl.carousel.js',
        'js/owl-slider/dist/owl.carousel2.thumbs.js',
        'js/swiper-slider/js/swiper.js',
        'js/jquery.matchHeight-min.js',
        'js/scripts.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
    
}
