<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\FrameExtension\models\FrameExtension */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Frame Extension',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frame Extensions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="frame-extension-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
