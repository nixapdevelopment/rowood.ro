<?php

namespace app\modules\Anschraubdichtung;

/**
 * anschraubdichtung module definition class
 */
class Anschraubdichtung extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Anschraubdichtung\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
