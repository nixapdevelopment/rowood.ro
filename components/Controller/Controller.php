<?php

namespace app\components\Controller;

use yii\web\Controller as YiiController;

class Controller extends YiiController
{
    
    public function init()
    {
        parent::init();
    }
    
}
