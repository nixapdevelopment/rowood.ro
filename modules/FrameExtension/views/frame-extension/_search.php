<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\FrameExtension\models\FrameExtensionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="frame-extension-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Size') ?>

    <?= $form->field($model, 'Left') ?>

    <?= $form->field($model, 'Rigth') ?>

    <?= $form->field($model, 'Top') ?>

    <?php // echo $form->field($model, 'Bottom') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
