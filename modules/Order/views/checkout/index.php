<?php
use app\views\themes\rowood\assets\RowoodAssets;
use app\modules\User\widgets\Login\Login;
use app\modules\User\widgets\Registration\Registration;
use yii\helpers\Url;

$bundle = RowoodAssets::register($this);
?>

<style>

    .login-box,.register-box{
        margin-top: 20px;
        margin-bottom: 20px;
        padding: 60px;
        border: 1px solid #333;
        border-radius:10px;
    }
    .box-title{
        font-size: 24px;
        font-weight: 800;
        color: #e3210d;
        margin-bottom:10px;
        display: block;
    }


</style>

<div class="container">
    <div id="checkout-steps">
        <a href="<?=Url::to('/admin/ecommerce/order/cart/view/')?>">
        <div class="checkout-block-step  previous" >
            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>Cos</div></a>
        <div class="checkout-block-step active ">
            <div class="checkout-step-circle active">2</div>Datele personale</div>
        <div class="checkout-block-step  ">
            <div class="checkout-step-circle ">3</div>Transportare</div>
        <div class="checkout-block-step  ">
            <div class="checkout-step-circle ">4</div>Plata</div>
        <div class="checkout-block-step  ">
            <div class="checkout-step-circle ">5</div>Comanda</div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-6">

            <div class="login-box">
                <span class="box-title">Logare</span><br>
                <?=Login::widget(['redirect' => '/admin/ecommerce/order/checkout/transportation/?orderID='.$orderID])?>
            </div>
        </div>

        <div class="col-md-6">

            <div class="register-box">
                <span class="box-title">Înregistrare</span><br>
                <?=Registration::widget(['redirect' => '/admin/ecommerce/order/checkout/transportation/?orderID='.$orderID])?>
            </div>
        </div>
    </div>
</div>
