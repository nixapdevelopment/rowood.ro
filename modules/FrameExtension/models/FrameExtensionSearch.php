<?php

namespace app\modules\FrameExtension\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\FrameExtension\models\FrameExtension;

/**
 * FrameExtensionSearch represents the model behind the search form about `app\modules\FrameExtension\models\FrameExtension`.
 */
class FrameExtensionSearch extends FrameExtension
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Size', 'Left', 'Rigth', 'Top', 'Bottom'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FrameExtension::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Size' => $this->Size,
            'Left' => $this->Left,
            'Rigth' => $this->Rigth,
            'Top' => $this->Top,
            'Bottom' => $this->Bottom,
        ]);

        return $dataProvider;
    }
}
